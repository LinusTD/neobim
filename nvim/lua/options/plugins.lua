vim.cmd("packadd packer.nvim")

return require("packer").startup(function()
	-- Packer
	use("wbthomason/packer.nvim")

	-- Theme
	use("sainnhe/gruvbox-material")

	-- Statusline
	use({
		"hoob3rt/lualine.nvim",
		config = function()
			require("configs.lualine")
		end
	})

	-- Telescope
	use({
		"nvim-telescope/telescope.nvim",
		requires = {{"nvim-lua/plenary.nvim"}},
		config = function()
			require("configs.telescope")
		end
	})

	-- Greeter
	use({
		"goolord/alpha-nvim",
		config = function()
			require("configs.alpha")
		end
	})

	-- Indentline
	use({
		"lukas-reineke/indent-blankline.nvim",
		config = function()
			require("indent_blankline").setup {
				buftype_exclude = {"terminal", "nofile"}
			}
		end
	})

	-- HTTP Client
	use({
		"NTBBloodbath/rest.nvim",
		requires = { "nvim-lua/plenary.nvim" },
		config = function()
			require("configs.rest")
		end
	})

	-- Treesitter
	use({
		"nvim-treesitter/nvim-treesitter",
		run = ":TSUpdate",
		config = function()
			require("configs.treesitter")
		end
	})

	-- LSP Config
	use({
		"williamboman/mason.nvim",
		config = function()
			require("mason").setup()
		end
	})

	use({
		"WhoIsSethDaniel/mason-tool-installer.nvim",
		config = function()
			require("configs.mason-auto-install")
			require("configs.lsp").setup()
		end
	})

	use({
		"williamboman/mason-lspconfig.nvim",
		config = function()
			require("mason-lspconfig").setup()
		end,
		requires = {
			"neovim/nvim-lspconfig",
		},
	})

	-- Auto completion
	use({
		"ms-jpq/coq_nvim",
		branch = "coq",
		requires = {
			'ms-jpq/coq.artifacts',
			branch = "artifacts"
		}
	})

	-- Orgmode
	use({
		"nvim-orgmode/orgmode",
		config = function()
			require("configs.orgmode")
		end
	})

	-- Git
	use({
		'TimUntersberger/neogit',
		requires = 'nvim-lua/plenary.nvim'
	})
end)

