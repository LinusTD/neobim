-- Settings
vim.g.gruvbox_material_background = "soft"
vim.g.gruvbox_material_show_eob = 0

-- Colorscheme
vim.cmd("colorscheme gruvbox-material")

