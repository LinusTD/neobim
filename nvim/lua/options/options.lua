local o = vim.o
local wo = vim.wo
local opt = vim.opt

-- General settings
o.ruler = false
o.splitright = true
o.splitbelow = true
o.ignorecase = true
o.background = "dark"
o.termguicolors = true
o.hidden = true
o.updatetime = 300
o.scrolloff = 5
o.sidescrolloff = 5
o.showmode = false
o.clipboard = "unnamedplus"
o.mouse = "a"

-- Column settings
wo.number = true
wo.relativenumber = true
wo.signcolumn = "number"
wo.wrap = false

-- Tab settings
opt.expandtab = false
opt.tabstop = 4
opt.shiftwidth = 4
opt.autoindent = true
opt.expandtab = true
opt.smartindent = true
opt.smarttab = true
opt.cindent = true
opt.breakindent = true
opt.shiftround = true
vim.cmd("set noexpandtab")

-- Diagnostics
vim.diagnostic.config({
	virtual_text = false, -- Turn off inline diagnostics
})

