-- Puts a 10ms delay on syntax highlighting
-- & filetype detection
vim.cmd([[
	syntax off
	filetype plugin indent off
]])

vim.opt.shadafile = "NONE"

require("optimization.core")

vim.opt.shadafile = ""

vim.defer_fn(function()
	vim.cmd([[
	syntax on
	filetype plugin indent on
	PackerLoad nvim-treesitter
	]])
end, 10)

