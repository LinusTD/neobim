require("nvim-treesitter.configs").setup({
	highlight = {
		enable = true,
		disable = {},
	},
	indent = {
		enable = true,
		disable = {},
	},
	ensure_installed = {
		"c",
		"c_sharp",
		"kotlin",
		"java",
		"go",
		"rust",
		"nix",

		"html",
		"latex",
		"markdown",
		"json",
		"yaml",
		"toml",
		"http",

		"javascript",
		"typescript",
		"python",
		"svelte",
		"vue",
		"bash",
		"lua",
	},
})

local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
parser_config.tsx.filetype_to_parsername = { "javascript", "typescript.tsx" }

