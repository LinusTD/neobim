require("lualine").setup {
	options = {
		icons_enabled = false,
		theme = "gruvbox-material",
		component_separators = { left = "", right = ""},
		section_separators = { left = "", right = ""},
		disabled_filetypes = {},
		always_divide_middle = true,
	},
	sections = {
		lualine_a = {"mode"},
		lualine_b = {},
		lualine_c = {"filename"},
		lualine_x = {"diagnostics"},
		lualine_y = {"location"},
		lualine_z = {},
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = {},
		lualine_x = {},
		lualine_y = {},
		lualine_z = {}
	},
	tabline = {},
	extensions = {}
}

