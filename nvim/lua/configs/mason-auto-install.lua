require("mason-tool-installer").setup({
	ensure_installed = {
		-- LSP
		"clangd",
		"omnisharp",
		"kotlin-language-server",
		"arduino-language-server",
		"jdtls",
		"gopls",
		"rust-analyzer",
		"rnix-lsp",

		"html-lsp",
		"ltex-ls",
		"marksman",
		"json-lsp",
		"yaml-language-server",
		"taplo",
		"css-lsp",

		"typescript-language-server",
		"pyright",
		"svelte-language-server",
		"vue-language-server",
		"angular-language-server",
		"bash-language-server",
		"lua-language-server",

		"grammarly-languageserver"
	},

	auto_update = false,
	run_on_start = true,
	start_delay = 3000,
})

