# Welcome
```
  █▀█▒                 █▀█▒    
 █  ██▒               █  ██▒   
 █ ███▒               █ ███▒   
  ███▒  ▆   ▆          ███▒    
                            █▒ 
 ▄██████████████████▄        █▒
         ▄▄▄▄▄    ▀███████▀  █▒
                            █▒ 
```


Here you can find my personal minimalist Neovim config.

For your own use you might want to change ``alpha.lua`` & ``orgmode.lua`` to point Orgmode towards your own files.

``lsp.lua``, ``mason-auto-install.lua`` & ``treesitter.lua`` automatically sets up the LSP's I use. Adjust for your own use.



## File structure
```

neobim/
├── nvim
│   ├── init.lua
│   └── lua
│       ├── configs
│       │   ├── alpha.lua
│       │   ├── lsp.lua
│       │   ├── lualine.lua
│       │   ├── mason-auto-install.lua
│       │   ├── orgmode.lua
│       │   ├── rest.lua
│       │   ├── telescope.lua
│       │   └── treesitter.lua
│       ├── optimization
│       │   ├── core.lua
│       │   └── delay.lua
│       └── options
│           ├── binds.lua
│           ├── colorscheme.lua
│           ├── options.lua
│           ├── packer.lua
│           └── plugins.lua
└── README.md
```


## Packer
Packer gets installed when running the config for the first time.

To install the plugins run:
```
:PackerSync
```
